package main

import (
	"bytes"
	"database/sql"
	"encoding/base64"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const (
	MB = 1 << 20
)

var db *sql.DB

func main() {
	godotenv.Load(".env")
	port := os.Getenv("PORT")
	db = DBConfig()
	http.HandleFunc("/upload", handler)

	fmt.Println("Server start at port ", port)
	http.ListenAndServe(":"+port, nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
	// only method post allowed
	case "POST":
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		if err := r.ParseMultipartForm(1024); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		auth := r.FormValue("auth")
		os.Setenv("AUTH", auth)
		fmt.Println("AUTH==", os.Getenv("AUTH"))

		uploadedFile, header, err := r.FormFile("data")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer uploadedFile.Close()

		// CHECK IF FILE UPLOADED MUST BE LOWER THAN 8 MB
		if header.Size > 8*MB {
			http.Error(w, "File upload must lower than 8 MB", http.StatusInternalServerError)
			return
		}

		data, err := ioutil.ReadAll(uploadedFile)
		if err != nil {
			log.Println(err)
			return
		}

		contentType := http.DetectContentType(data)

		// VALIDATE CONTENT TYPE
		switch contentType {
		case "image/jpeg", "image/png":
			img, err := jpeg.Decode(bytes.NewReader(data))
			if err != nil {
				fmt.Fprintf(w, "unable to decode jpeg: %w", err)
				return
			}

			var buf bytes.Buffer
			if err := png.Encode(&buf, img); err != nil {
				fmt.Fprintf(w, "unable to encode png: %w", err)
				return
			}
			data = buf.Bytes()
		default:
			http.Error(w, "", http.StatusForbidden)
			fmt.Fprintf(w, "unsupported content type: %s", contentType)
			return
		}

		insertData(db, header.Filename, contentType, int(header.Size))
		imgBase64Str := base64.StdEncoding.EncodeToString(data)

		dir, err := os.Getwd()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fileLocation := filepath.Join(dir, "temp")
		targetFile, err := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer targetFile.Close()

		if _, err := io.Copy(targetFile, strings.NewReader(imgBase64Str)); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Write([]byte("done"))
	default:
		fmt.Fprintf(w, "Request method %s is not supported", r.Method)
	}
}

func DBConfig() *sql.DB {
	// The database is called testDb
	db, err := sql.Open("mysql", os.Getenv("DB_URL"))

	// if there is an error opening the connection, handle it
	if err != nil {
		panic(err.Error())
	}

	createTable(db)
	return db
}

func createTable(db *sql.DB) {
	query := `CREATE TABLE IF NOT EXISTS files (
		id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,		
		filename TEXT,
		content_type TEXT,
		file_size integer 		
	  );`
	// SQL Statement for Create Table

	log.Println("Create files table...")
	//_, err := db.Exec(query)
	//if err != nil {
	//	panic(err)
	//}
	statement, err := db.Prepare(query) // Prepare SQL Statement
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec() // Execute SQL Statements
	log.Println("files table created")
}

// We are passing db reference connection from main to our method with other parameters
func insertData(db *sql.DB, filename, contentType string, size int) {
	log.Println("Inserting file record ...")
	insertSQL := `INSERT INTO files(filename, content_type, file_size) VALUES (?, ?, ?)`
	statement, err := db.Prepare(insertSQL) // Prepare statement.
	// This is good to avoid SQL injections
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = statement.Exec(filename, contentType, size)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
