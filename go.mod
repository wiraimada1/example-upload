module example.com/upload

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
)
